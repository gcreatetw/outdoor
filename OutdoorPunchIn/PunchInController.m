//
//  PunchInController.m
//  OutdoorPunchIn
//
//  Created by NeilChen on 2015/10/19.
//  Copyright © 2015年 NeilChen. All rights reserved.
//

#import "PunchInController.h"
@import GoogleMaps;
#import <SVProgressHUD.h>


@interface PunchInController ()
@property (weak, nonatomic) IBOutlet UIView *map;
@end

@implementation PunchInController{
    
    GMSMapView *mapView;
    NSString *userName,*userCard;
    NSDictionary *jsonData;
    GMSCameraPosition *camera;
    NSArray *jobLocation;
    
    __weak IBOutlet UILabel *cardId;
    __weak IBOutlet UILabel *name;
    __weak IBOutlet UILabel *version;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    version.text =[NSString stringWithFormat:@"ver. %@",[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]];
    
    NSUserDefaults *sd = [NSUserDefaults standardUserDefaults];
    
    userName = [sd objectForKey:@"_NAME"];
    userCard = [sd objectForKey:@"_CARD_ID"];
    
    name.text = [NSString stringWithFormat:@"員工姓名:%@",userName];
    cardId.text = [NSString stringWithFormat:@"員工代號:%@",userCard];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
        
    }
    else{
        [locationManager startUpdatingLocation];
    }
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
}



-(void)viewDidLayoutSubviews{
    
    if (mapView == nil) {
        
        //顯示照相機位置
        camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude                                                       longitude:locationManager.location.coordinate.longitude zoom:16];
        
        mapView = [GMSMapView mapWithFrame:self.map.bounds camera:camera];
        mapView.settings.myLocationButton = YES;
        mapView.myLocationEnabled = YES;
        [self.map addSubview:mapView];
        
        
        if (locationManager.location.coordinate.latitude >0) {
            
            //增加使用者點位
            GMSMarker *marker = [[GMSMarker alloc]init];
            marker.position = locationManager.location.coordinate;
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.icon = [UIImage imageNamed:@"user_pin"];
            marker.map =mapView;
        }
        
        NSString *url;
        url =[NSString stringWithFormat:@"/api/PunchIn/LocationInfo"];
        
        NetBO *net = [[NetBO alloc]init];
        NSString *data = [net HttpGet:url];
        
        
        jsonData = [NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        if ([[jsonData objectForKey:@"Result"] boolValue]) {
            
            jobLocation = jsonData[@"Location"];
            
            
            //取得工廠位置
            for (int i=0; i<[jobLocation count]; i++) {
                
                //檢查是否是空值
                double checkY = [[jobLocation[i] objectForKey:@"YPoint"] doubleValue];
                //不是才顯示
                if (checkY>0) {
                    
                    GMSMarker *marker = [[GMSMarker alloc]init];
                    marker.position= CLLocationCoordinate2DMake([[jobLocation[i] objectForKey:@"XPoint"] doubleValue], [[jobLocation[i] objectForKey:@"YPoint"] doubleValue]);
                    
                    
                    marker.appearAnimation = kGMSMarkerAnimationPop;
                    marker.icon = [UIImage imageNamed:@"ct_pin.png"];
                    marker.map =mapView;
                    
                    CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake([[jobLocation[i] objectForKey:@"XPoint"]doubleValue],[[jobLocation[i] objectForKey:@"YPoint"]doubleValue]);
                    GMSCircle *circle = [GMSCircle circleWithPosition:circleCenter radius:1000];
                    circle.fillColor =[UIColor colorWithRed:1 green:0 blue:0 alpha:0.25];
                    circle.strokeWidth = 0;
                    circle.map =mapView;
                }
                
            };
            
        }
        
    }
    
    
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    CLLocation *currentLocation = [locations objectAtIndex:0];
    
    double lat = currentLocation.coordinate.latitude;
    double lon = currentLocation.coordinate.longitude;
    
    //顯示照相機位置
    camera = [GMSCameraPosition cameraWithLatitude:lat                                                   longitude:lon zoom:18];
    //移動照相機
    [mapView animateToCameraPosition:camera];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}



- (IBAction)punchIn:(UIButton *)sender {
    
    [SVProgressHUD show];
    
    NSString *url;
    double distance=999999;
    NSString *jobNo;
    
    
    CLLocation *userLocaiton = locationManager.location;
    
    
    for (int i=0; i<[jobLocation count]; i++) {
        
        CLLocation *factoryLocation = [[CLLocation alloc] initWithLatitude:[[jobLocation[i] objectForKey:@"XPoint"] doubleValue] longitude:[[jobLocation[i] objectForKey:@"YPoint"] doubleValue]];
        
        double newDistance = [factoryLocation distanceFromLocation:userLocaiton];
        if (newDistance<distance) {
            distance=newDistance;
            jobNo = [jobLocation[i] objectForKey:@"JobNo"];
            NSLog(@"distance:%f,jobNo:%@",distance,jobNo);
        }
    }
    
    int changeDisatnce = distance;
    
    //代表使用者與最近的工地超過一公里
    if (changeDisatnce <= 1000) {
        
         url =[NSString stringWithFormat:@"/api/PunchIn/PunchInDataSave?lat=%f&lng=%f&distance=%d&jobNo=%@&userNumber=%@",userLocaiton.coordinate.latitude,userLocaiton.coordinate.longitude,0,jobNo,userCard];
        
        NSLog(@"%@",url);
        
        NetBO *net = [[NetBO alloc]init];
        NSString *data = [net HttpGet:url];
        
        NSDictionary *result =[NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        //判斷回傳是否是成功
        if ([[result objectForKey:@"Result"] boolValue]) {
            
            GMSMarker *marker = [[GMSMarker alloc]init];
            marker.position = userLocaiton.coordinate;
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.icon = [UIImage imageNamed:@"user_punchin"];
            marker.map =mapView;
            [SVProgressHUD dismiss];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"成功" message:@"您已經成功打卡" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            [SVProgressHUD dismiss];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"系統錯誤，請從新打卡謝謝。" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }else{
        
        url =[NSString stringWithFormat:@"/api/PunchIn/PunchInDataSave?lat=%f&lng=%f&distance=%d&jobNo=%@&userNumber=%@",userLocaiton.coordinate.latitude,userLocaiton.coordinate.longitude,0,@"0",userCard];
        
        NSLog(@"%@",url);
        
        NetBO *net = [[NetBO alloc]init];
        NSString *data = [net HttpGet:url];
        
        NSDictionary *result =[NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        //判斷回傳是否是成功
        if ([[result objectForKey:@"Result"] boolValue]) {
            
            GMSMarker *marker = [[GMSMarker alloc]init];
            marker.position = userLocaiton.coordinate;
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.icon = [UIImage imageNamed:@"user_punchin"];
            marker.map =mapView;
            [SVProgressHUD dismiss];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"成功" message:@"您已經成功打卡" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            [SVProgressHUD dismiss];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"系統錯誤，請從新打卡謝謝。" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
}


@end

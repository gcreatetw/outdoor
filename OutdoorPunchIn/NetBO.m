//
//  NetBO.m
//  Taifung
//
//  Created by Liao Kevin on 11/7/28.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "NetBO.h"



@implementation NetBO

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(NSString*) HttpXmlPost:(NSString *)content{
    
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setTimeoutInterval:20.0];//設定timeout的時間。
	[request setURL:[NSURL URLWithString:SERVER_URL]];
	[request setHTTPMethod:@"POST"];
    
    //set headers
	[request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    
    
	//create the body
	NSMutableData *postBody = [NSMutableData data];
	[postBody appendData:[[NSString stringWithFormat:@"%@",content] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //post
	[request setHTTPBody:postBody];
    
	//get response
	NSHTTPURLResponse* urlResponse = nil;  
	NSError *error = [[NSError alloc] init];  
	NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];  
	NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"Response Code: %d", (int)[urlResponse statusCode]);
	if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300) {
		NSLog(@"Response: %@", result);
        return result;
	}
    return nil;
}

-(NSString*) HttpGet:(NSString *)url{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setTimeoutInterval:30.0];//設定timeout的時間。
	[request setURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@",SERVER_URL,url]]];
	[request setHTTPMethod:@"GET"];
    
    //set headers
	[request addValue:@"text/plain; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    
	//get response
	NSHTTPURLResponse* urlResponse = nil;
	NSError *error = [[NSError alloc] init];
	NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
	NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"Response Code: %d", (int)[urlResponse statusCode]);
	if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300) {
		NSLog(@"Response: %@", result);
        return result;
	}
    return nil;
}

-(NSString *)getMacAddress
{
    //NSString *adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    //return adId;
    return @"";
}
@end

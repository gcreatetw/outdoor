//
//  NetBO.h
//  Taifung
//
//  Created by Liao Kevin on 11/7/28.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <sys/sysctl.h>
#import <net/if.h>
#import <net/if_dl.h>
#import  <AdSupport/AdSupport.h>

#define SERVER_URL @"http://59.124.150.10/"
@interface NetBO : NSObject
-(NSString*) HttpXmlPost:(NSString*)content;
-(NSString*) getMacAddress;
-(NSString*) HttpGet:(NSString*)url;
@end

//
//  LoginController.m
//  OutdoorPunchIn
//
//  Created by NeilChen on 2015/10/15.
//  Copyright © 2015年 NeilChen. All rights reserved.
//

#import "LoginController.h"
#import "PunchInController.h"
#import <SVProgressHUD.h>


@interface LoginController ()

@property (weak, nonatomic) IBOutlet UILabel *version;


@end



@implementation LoginController{
    
    //密碼
    __weak IBOutlet UITextField *passText;
    //帳號
    __weak IBOutlet UITextField *accountText;
    NSDictionary *jsonData;
    NSString *account,*password;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //取得系統版本
    NSString *version =[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    //顯示當前版本
    self.version.text =[NSString stringWithFormat:@"ver. %@",version];
    
    //設定該欄位會以密碼欄位顯示 例：*****1
    passText.secureTextEntry=YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (IBAction)enter:(UIButton *)sender {
    
    
    
    NSLog(@"account:%@ , pass:%@",accountText.text,passText.text);
    NSString *url;
    NetBO *net = [[NetBO alloc]init];
    
    //檢查帳號密碼是否為空值
    if (![accountText.text isEqualToString:@""] && ![passText.text isEqualToString:@""]) {
        //不為空值才會進入這裡
        
        account = [accountText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        password = [passText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        url = [NSString stringWithFormat:@"/api/Login/UserLogin?account=%@&password=%@",account,password];
        
        NSLog(@"url:%@",url);
        
        NSString *data = [net HttpGet:url];
        NSLog(@"data:%@",data);
        
        NSError *error;
        
        
        //判斷網路是否有異常
        if (error!=nil) {
            
            NSLog(@"error:%@",error);
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"資料連線異常" message:@"請檢查網路連線是否正常，再試一次。" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            //呼叫Loading Dialog
            [SVProgressHUD show];
        }
        
        
        jsonData = [NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        //判斷帳號密碼是否錯誤
        if ([[jsonData objectForKey:@"Result"] intValue]==0) {
            
            //停止Dialog
            [SVProgressHUD dismiss];
            [self showAlertDialog:[jsonData objectForKey:@"Message"]];
            
        }else{
            
            //判斷該員工是否已經離職
            if (![[NSString stringWithFormat:@"%@", [[jsonData objectForKey:@"User"] objectForKey:@"DUT_OT_D"]] isEqualToString:@"<null>"]) {
                //停止Dialog
                [SVProgressHUD dismiss];
                [self showAlertDialog:@"帳號過期"];
                accountText.text = @"";
                passText.text = @"";
                return;
            }
            
            NSString *name,*pwd,*cardId;
            
            name =[[jsonData objectForKey:@"User"]objectForKey:@"NAME"];
            pwd =[[jsonData objectForKey:@"User"]objectForKey:@"USER_PWD"];
            cardId =[[jsonData objectForKey:@"User"]objectForKey:@"SAL_NO"];
            
            NSLog(@"NAME:%@",name);
            NSLog(@"USER_PWD:%@",pwd);
            NSLog(@"SAL_NO:%@",cardId);
            
            //寫入(紀錄)在手機的暫存檔案，登入成功就會記錄起來
            NSUserDefaults *sd = [NSUserDefaults standardUserDefaults];
            [sd setObject:name forKey:@"_NAME"];
            [sd setObject:pwd forKey:@"_PWD"];
            [sd setObject:cardId forKey:@"_CARD_ID"];
            [sd synchronize];
            //停止Dialog
            [SVProgressHUD dismiss];
            
            //轉跳至打卡頁面
            PunchInController *punchIn = [[PunchInController alloc]init];
            //翻書特效
            //punchIn.modalTransitionStyle = UIModalTransitionStylePartialCurl;
            //一般上下轉換
            punchIn.modalTransitionStyle =UIModalTransitionStyleCoverVertical;
            [self presentViewController:punchIn animated:YES completion:^{
                
            }];
            
            
        }
        
        
    }else{
        
        //9.0之後的寫法，需要分別寫下 controller 與 action
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"帳號或秘密碼不可為空" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            //這邊可以設定動作
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        //將該Action 加入Controller
        [alert addAction:ok];
        //在該ViewController顯示該筆 Alert視窗
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}

/**顯示Alert視窗*/
-(void) showAlertDialog: (NSString *) messageStr  {
 
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"登入錯誤" message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}


@end

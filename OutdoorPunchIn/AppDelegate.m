//
//  AppDelegate.m
//  OutdoorPunchIn
//
//  Created by NeilChen on 2015/10/15.
//  Copyright © 2015年 NeilChen. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginController.h"
#import "PunchInController.h"
#import <AFNetworking.h>

@import GoogleMaps;

@interface AppDelegate ()

@end

@implementation AppDelegate{
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    //google map api key
    // henry帳號底下：AIzaSyB1Wga1i0JQaS0C6WSgQBY-bJmkDNQE6OY
    [GMSServices provideAPIKey:@"AIzaSyB1Wga1i0JQaS0C6WSgQBY-bJmkDNQE6OY"];
    
    
    //檢查是否有新版可以安裝
    [self checkVersion:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]];
    
    return YES;
}

-(void) login{
    
    //讀取記錄檔
    NSUserDefaults *sd = [NSUserDefaults standardUserDefaults];
    
    //判別是否有紀錄，若有則直接到打卡頁面
    if ([[sd objectForKey:@"_NAME"] length]>0) {
        
        NSLog(@"%@",[sd objectForKey:@"_NAME"]);
        
        //打卡頁面
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = [[PunchInController alloc]initWithNibName:@"PunchInController" bundle:nil];
        
    }else{
        
        //沒有則跳進Login頁面
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
        self.window.rootViewController = [[LoginController alloc]initWithNibName:@"LoginController" bundle:nil];
    }
    [self.window makeKeyAndVisible];
    
}

//檢查是否有可以更新的版本
-(void) checkVersion:(NSString *)version{
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // check version url
    NSString *versionURL = @"https://dl.dropbox.com/s/zi7zs9eco7ifm7y/version_ios.json";
    
    //讀取版本資料 set timeout 30s [version 2.6.3]
    
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
//    
//    [manager GET:versionURL
//      parameters:nil
//         success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         NSLog(@"JSON %@", responseObject );
//         
//         NSDictionary *json = responseObject;
//         float getVersion = [[json objectForKey:@"version"] floatValue];
//         float nowVersion = [version floatValue];
//         
//         //代表有新的版本
//         if (getVersion > nowVersion) {
//             
//             NSLog(@"有新版本");
//             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:@"檢查有新的版本" preferredStyle:UIAlertControllerStyleAlert];
//             //更新按鈕
//             UIAlertAction *update = [UIAlertAction actionWithTitle:@"更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                 
//                 //更新連結
//                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://59.124.150.10/Download/OutdoorPunchIn.html"]];
//                 
//             }];
//             //取消按鈕
//             UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                 [alert dismissViewControllerAnimated:YES completion:nil];
//                 
//                 //跳至登入畫面
//                 [self login];
//             }];
//             
//             [alert addAction:update];
//             [alert addAction:cancel];
//             
//             //顯示在畫面上
//             [self.window makeKeyAndVisible];
//             [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
//             
//         }else{
//             NSLog(@"無更新版本");
//             //跳至登入畫面
//             [self login];
//         }
//         
//         
//     }
//         failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         NSLog(@"fail %@", error );
//     }];
    
    
    
    //AFNetworking version 3.0的寫法
    
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
        [manager GET:versionURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
    
            NSLog(@"JSON: %@", responseObject);
            NSDictionary *json = responseObject;
    
            float getVersion = [[json objectForKey:@"version"] floatValue];
            float nowVersion = [version floatValue];
    
            //代表有新的版本
//            if (getVersion > nowVersion) {
//
//                NSLog(@"有新版本");
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:@"檢查有新的版本" preferredStyle:UIAlertControllerStyleAlert];
//                //更新按鈕
//                UIAlertAction *update = [UIAlertAction actionWithTitle:@"更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//                    //更新連結
//                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://59.124.150.10/Download/OutdoorPunchIn.html"]];
//
//                }];
//                //取消按鈕
//                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    [alert dismissViewControllerAnimated:YES completion:nil];
//
//                    //跳至登入畫面
//                    [self login];
//                }];
//
//                [alert addAction:update];
//                [alert addAction:cancel];
//
//                //顯示在畫面上
//                [self.window makeKeyAndVisible];
//                [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
//
//            }else{
//                NSLog(@"無更新版本");
//                //跳至登入畫面
//                [self login];
//            }
            
            [self login];
    
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

//
//  PunchInController.h
//  OutdoorPunchIn
//
//  Created by NeilChen on 2015/10/19.
//  Copyright © 2015年 NeilChen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetBO.h"
#import <CoreLocation/CoreLocation.h>

@interface PunchInController : UIViewController<CLLocationManagerDelegate>{
    
    CLLocationManager *locationManager;
}

@end

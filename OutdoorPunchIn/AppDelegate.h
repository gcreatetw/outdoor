//
//  AppDelegate.h
//  OutdoorPunchIn
//
//  Created by NeilChen on 2015/10/15.
//  Copyright © 2015年 NeilChen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

